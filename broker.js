var mosca = require("mosca");
// const SECURE_KEY = __dirname + "/secure/reconoser-access-control.pem";
// const SECURE_CERT = __dirname + "/secure/reconoser-access-control-cert.pem";
const topics = ["managementDevice"];
const config = require('./config');
let usersAuthorized = [];
var settings = {
    port: 1883,
    logger: {
        name: "secureExample",
        level: 40,
    },
    // secure: {
    //     port: 8443,
    //     keyPath: SECURE_KEY,
    //     certPath: SECURE_CERT,
    // },
};

// Accepts the connection if the username and password are valid
var authenticate = function (client, username, password, callback) {
    var authorized =
        username === config.USERNAME &&
        password.toString() === config.PASS;
    if (authorized) {
        client.user = username;
        usersAuthorized.push(client.user);
        console.log("New client authorized");
    }
    callback(null, authorized);
};

// the username from the topic and verifing it is the same of the authorized user
var authorizePublish = function (client, topic, payload, callback) {
    callback(
        null,
        topics.includes(topic) && usersAuthorized.includes(client.user)
    );
};

// the username from the topic and verifing it is the same of the authorized user
var authorizeSubscribe = function (client, topic, callback) {
    callback(
        null,
        topics.includes(topic) && usersAuthorized.includes(client.user)
    );
};

var server = new mosca.Server(settings);

function initilize() {
    server.on("ready", setup);
}

function setup() {
    server.authenticate = authenticate;
    server.authorizePublish = authorizePublish;
    server.authorizeSubscribe = authorizeSubscribe;
    console.log(`Broker listening`);
}

server.on("clientConnected", function (client) {
    console.log(
        `client connected with id ${client.id} and user ${client.user}`
    );
});

module.exports = {
    initilize,
};
