var mqtt = require("mqtt");
const subscriber = require("./subscriber");
const config = require("./config");
const publishModel = require("./publish.model");
var client = mqtt.connect(`mqtt://${config.IP_SERVER}`, {
    clientId: "Publisher",
    username: config.USERNAME,
    password: config.PASS,
});

function publish(body, callback) {
    const topic = "managementDevice";
    let counterChangeState = 0;
    let response = {
        success: false,
        payload: null,
        message: null,
        error: null,
    };

    // subscriber.subscribe("responseDevice", function (err, result) {
    //     if (err) return err;
    //     console.log("Se ha suscrito al tema " + result.topic);
    // });

        try {
            if (client.connected) {
                client.publish(topic, JSON.stringify(body), async function (
                    err
                ) {
                    if (err) {
                        response.error = err;
                        response.message = `Hubo un error al publicar en el tema ${topic}`;
                        return callback(response);
                    }

                    const publishData = {
                        topic,
                        tag: body.tag,
                        state: "inProccess",
                        deviceId: body.device_id,
                    };

                    await publishModel.create(publishData);

                    function publishResponse(publishResolved, result) {
                        if(counterChangeState === 25 && !publishResolved && body.mqtt_operate_id !== 7){
                            response.success = false;
                            response.message = `No se logró obtener respuesta de la publicación`;
                            response.error = 'TimeOut'
                            return callback(response);
                        }

                        if (publishResolved) {
                            response.success = true;
                            response.message = `Se publicó en el tema ${topic}`;
                            response.payload = result.data;
                            return callback(response);
                        }

                        publishModel
                            .findOne({
                                deviceId: body.device_id,
                                tag: body.tag,
                            })
                            .then((data) => {
                                console.log('------------------ Data DB ------------------')
                                console.log(`revisando si cambio de estado... ${counterChangeState}`);
                                console.log(`data_device_id ${data.deviceId}`)
                                console.log(`body_device_id ${body.device_id}`)
                                console.log(`data_topic ${data.topic}`)
                                publishResolved = data && data.state === "resolved";
                                setTimeout(function () {
                                    counterChangeState ++;
                                    publishResponse(publishResolved, data);
                                }, 1000);
                            })
                            .catch((error) => {
                                response.error = error;
                                response.message = `Hubo un error al publicar en el tema ${topic}`;
                                return callback(response);
                            });
                    }

                    setTimeout(function () {
                        publishResponse(false);
                    }, 1000);
                });
            }else{
                response.error = 'Client Not Connected';
                response.message = `El cliente no se encuentra conectado al broker`;
                return callback(response);
            }

    } catch (error) {
        console.log("error", error);
        response.error = err;
        response.message = `Hubo un error al publicar en el tema ${topic}`;
        return callback(response);
    }
}

module.exports = {
    publish,
};
