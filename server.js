const express = require("express");
const mongoose = require("mongoose");
const validateForm = require('./middleware/fromValidations')();
const broker = require("./broker");
const subscriber = require("./subscriber");
const publisher = require("./publisher");
const config = require('./config');
const bodyParser = require("body-parser");
const app = express();

const mongooseOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
};

app.use(bodyParser.json());

app.delete("/deleteAllPersonnel", (req, res) => {
    const { device_token, device_id } = req.body;
    const body = {
        mqtt_cmd: 1,
        mqtt_operate_id: 7,
        device_token,
        device_id,
        tag: String(new Date().getTime()),
        piclib_manage: 1,
    };
    publisher.publish(body, function (response) {
        res.status(200).json(response);
    });
});

app.delete("/deleteSinglePhoto/:device_id", (req, res) => {
    const { device_id } = req.params;
    const { device_token, param} = req.body;
    const body = {
        mqtt_cmd: 1,
        mqtt_operate_id: 7,
        device_token,
        device_id,
        tag: String(new Date().getTime()),
        piclib_manage: 3,
        param
    };
    publisher.publish(body, function (response) {
        res.status(200).json(response);
    });
});

app.post("/insertPersonnel", validateForm, (req, res) => {
    const { device_token, device_id, param } = req.body;
    const body = {
        mqtt_cmd: 1,
        mqtt_operate_id: 7,
        device_token,
        device_id,
        tag: String(new Date().getTime()),
        piclib_manage: 0,
        param,
    };

    console.log(`request ${body}`)
    publisher.publish(body, function (response) {
        res.status(200).json(response);
    });
});

app.get("/getPersonnelEnrolled", (req, res) => {
    const { device_token, device_id} = req.body;
    const body = {
        mqtt_cmd: 1,
        mqtt_operate_id: 7,
        device_token,
        device_id,
        tag: String(new Date().getTime()),
        piclib_manage: 4,
        page: 0
      }
    publisher.publish(body, function (response) {
        res.status(200).json(response);
    });
})

app.get("/getInfoDevice", function (req, res) {
    const { device_id, device_token } = req.query;
    const body = {
        mqtt_cmd: 2,
        device_id,
        tag: String (new Date().getTime()),
        device_token,
    };
    publisher.publish(body, function (response) {
        res.status(200).json(response);
    });
});

app.get("/subscribe", function (req, res) {
    const { topic } = req.query;
    subscriber.subscribe(topic, function (err, result) {
        if (err) return res.status(400).json(err);
        return res.status(200).json({
            message: `Se ha suscrito exitosamente al tema ${result.topic}`,
        });
    });
});

app.get("/", function (req, res) {
    res.status(200).json({
        message: `It's work`
    })
});

// Manejando errores HTTP 404 para solicitudes de contenido inexistente
app.use(function (req, res, next) {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});
// Manejo de errores, respuestas con codigo HTTP 500, HTTP 404
app.use(function (err, req, res, next) {
    console.log(err);

    if (err.status === 404) res.status(404).json({ message: "Not found" });
    else res.status(500).json({ message: "Error interno en el servidor!!" });
});
app.listen(1514, function () {
    mongoose
        .connect(config.URL_MONGO, mongooseOptions)
        .then(() => {
            console.log("DB up");
            console.log("Server up");
            console.log("Initilize broker...");
            subscriber.subscribe("responseDevice", function (err, result) {
                if (err) return res.status(400).json(err);
                console.log("Se ha suscrito al tema responseDevice");
            });
            broker.initilize();
        });
});
