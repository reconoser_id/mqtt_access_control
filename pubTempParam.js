var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://192.168.1.110');
var jsonStrTemperature = "{\"mqtt_cmd\":1,\"mqtt_operate_id\":6,\"device_token\":\"1208092819\",\"device_id\":\"7101955431980\",\"tag\":\"platform define\",\"temperature_fun\":{\"temp_dec_en\":true,\"stranger_pass_en\":false,\"make_check_en\":true,\"alarm_temp\":38.0,\"temp_comp\":0.0,\"record_save_time\":24,\"save_record\":true,\"save_jpeg\":true}}"
var jsonSinglePhoto = "{\"mqtt_cmd\":1,\"mqtt_operate_id\":7,\"device_token\":\"1208092819\",\"device_id\":\"7101955431980\",\"tag\":\"prueba foto uno\",\"piclib_manage\":0,\"param\":{\"lib_name\":\"reconoSER ID\",\"lib_id\":\"8\",\"server_ip\":\"192.168.1.104\",\"server_port\":80,\"pictures\":[{\"active_time\":\"2020/01/1 00:00:01\",\"user_id\":\"1\",\"user_name\":\"Camilo Parra\",\"end_time\":\"2020/12/30 23:59:59\",\"p_id\":\"null\",\"picture\":\"/camilo2.jpg\"}]}}"
var jsonMultiplePhoto = "{\"mqtt_cmd\":1,\"mqtt_operate_id\":7,\"device_token\":\"1208092819\",\"device_id\":\"7101955431980\",\"tag\":\"prueba foto dos\",\"piclib_manage\":0,\"param\":{\"lib_name\":\"reconoSER ID\",\"lib_id\":\"8\",\"server_ip\":\"192.168.1.104\",\"server_port\":80,\"pictures\":[{\"active_time\":\"2020/01/1 00:00:01\",\"user_id\":\"3\",\"user_name\":\"Jorge Castaño\",\"end_time\":\"2020/12/30 23:59:59\",\"p_id\":\"null\",\"picture\":\"/jorge.jpg\"},{\"active_time\":\"2020/01/1 00:00:01\",\"user_id\":\"2\",\"user_name\":\"Stephanie Varon\",\"end_time\":\"2020/12/30 23:59:59\",\"p_id\":\"null\",\"picture\":\"/stephanie2.jpg\"},{\"active_time\":\"2020/01/1 00:00:01\",\"user_id\":\"1\",\"user_name\":\"Camilo Parra\",\"end_time\":\"2020/12/30 23:59:59\",\"p_id\":\"null\",\"picture\":\"/Camilo%20Parra_desarrollo_a001.JPG\"}]}}"
var jsonDeletePhotos = "{\"mqtt_cmd\":1,\"mqtt_operate_id\":7,\"device_token\":\"1208092819\",\"device_id\":\"7101955431980\",\"tag\":\"eliminar fotos\",\"piclib_manage\":1}"


client.on('connect', function(){
   /* setInterval(function(){
        client.publish('myTopic',jsonStr);
        console.log('Message sent');
    },5000);*/
    client.publish('managementDevice',jsonSinglePhoto);
    console.log('Message sent');
});