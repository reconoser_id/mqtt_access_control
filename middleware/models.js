const Joi = require('joi');

const insertPersonnel = Joi.object().keys({
    device_token: Joi.string().required().label("Este campo es obligatorio"),
    device_id: Joi.string().required().label("Este campo es obligatorio"),
    param: Joi.object().keys({
        lib_name: Joi.string().required().label("Este campo es obligatorio"),
        lib_id: Joi.string().label("Este campo es obligatorio"),
        server_ip: Joi.string().label("Este campo es obligatorio"),
        server_port: Joi.number().label("Este campo es obligatorio"),
        pictures: Joi.array().items(Joi.object().keys({
            active_time: Joi.string().required().label("Este campo es obligatorio"),
            user_id: Joi.string().required().label("Este campo es obligatorio"),
            user_name: Joi.string().required().label("Este campo es obligatorio"),
            end_time: Joi.string().required().label("Este campo es obligatorio"),
            p_id: Joi.string(),
            picture: Joi.string().required().label("Este campo es obligatorio"),
        }))
    }),
});

module.exports = {
  '/insertPersonnel': insertPersonnel,
}