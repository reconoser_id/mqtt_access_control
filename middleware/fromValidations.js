const _ = require('lodash');
const Joi = require('joi');
const joiSchemas = require('./models');

// Joi validation options
const _validationOptions = {
  abortEarly: false, // abort after the last validation error
  allowUnknown: true, // allow unknown keys that will be ignored
  stripUnknown: true // remove unknown keys from the validated data
};

// return the validation middleware
module.exports = (useJoiError = false) => {
  return (req, res, next) => {
    const method = req.method
    const route = req.originalUrl
    const queryData = method === 'GET' ? req.query : req.body
    
    // get schema for the current route
    const _schema = _.get(joiSchemas, route)
    if(_schema) {
      const result = _schema.validate(queryData, _validationOptions)
        
        if(result.error) {
          //Joi error
          const joiError = {
            success: false,
            errors: {
              details: _.map(result.error.details, ({ context }) => ({
                validationForm: {
                  field: context.key,
                  message: context.label
                }
              }))
            }
          }
          // Send back the json response
          res.status(422).json(joiError)
        } 
        next()
      }
    }
}