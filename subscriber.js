const mqtt = require("mqtt");
const publishModel = require("./publish.model");
const config = require("./config");
const topicResponseDevice = "responseDevice";
const axios = require('axios');
var client = mqtt.connect(`mqtt://${config.IP_SERVER}`, {
    clientId: "Subscriber",
    username: config.USERNAME,
    password: config.PASS,
});

function subscribe(topic, callback) {
    let response = {
        success: false,
        payload: null,
        message: null,
        error: null,
    };
    if (topic) {
        client.subscribe(topic, function (err, granted) {
            if (err) {
                response.error = err;
                response.message = `No se logró suscribir al tema ${topic}`;

                callback(response);
            }

            client.on("message", async function (topic, message) {
                const response = JSON.parse(message.toString());
                console.log('response', response);

                if (topic === topicResponseDevice) {
                    const resultPublishModel = await publishModel.findOne({
                        deviceId: response.device_id,
                        tag: response.tag,
                    });

                    if (resultPublishModel) {
                        resultPublishModel.state = "resolved";
                        resultPublishModel.data = {
                            data: response.datas,
                            message: response.msg
                        }

                        await resultPublishModel.save();
                    }
                }

                // Recognition person notification
                if(response.tag === 'UploadPersonInfo'){
                    if(response.datas && response.datas.user_id !== '' && response.datas.name !== ''){

                        const requestConfig = {
                            headers: {
                                'Authorization': `Bearer ${config.OLIMPIA_TOKEN}`
                            }
                        }
                        const url = `${config.URL_OLIMPIA_BACK}:${config.OLIMPIA_PORT_BACK}/ClientApi/api/cliente/registrar-ingreso`

                        const data = {
                            deviceId: response.device_id,
                            personaId: response.datas.user_id,
                            temperatura: response.datas.temperature,
                            similar:response.datas.similar,
                            foto: response.datas.imageFile,
                            fechaValidacion: response.datas.time
                        }

                        axios.post(url, data, requestConfig)
                            .then(data => {
                                console.log('Se registró entrada ', data.statusText);
                            })
                            .catch(error => {

                                console.log('No se pudo registrar la entrada ', error.Errors);
                            })
                    }
                }
            });

            callback(false, granted[0]);
        });
    } else {
        response.message = `Debe proporcionar un tema al cual desea suscribirse`;
        response.error = `BadRquest`;
        callback(response);
    }
}

module.exports = {
    subscribe,
};
