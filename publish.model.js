// Load mongoose ODM
const mongoose = require("mongoose");
const uniqueValidator = require('mongoose-unique-validator');
//Initialize Schema
const Schema = mongoose.Schema;

const states = ["inProccess", "resolved"];
// Create schema with attributes
const PublishSchema = new Schema({
    topic: {
        type: String,
        trim: true,
        required: [true, "El tema es obligatorio"],
    },
    tag: {
        type: String,
        trim: true,
        unique: true,
        required: [true, "El tag es obligatorio"],
    },
    state: {
        type: String,
        trim: true,
        enum: states,
    },
    deviceId: {
        type: String,
        trim: true,
        required: [true, "El id del dispositivo es obligatorio"],
    },
    data: {
        type: Schema.Types.Mixed,
    },
});

PublishSchema.plugin(uniqueValidator, { message: 'El tag {VALUE} ya esta en uso.'});

// Export model
module.exports = mongoose.model("publish", PublishSchema);
